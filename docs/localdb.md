# データベース
--------------------------

# まず始めに
Preproductionのテスト結果はlocaldbにアップロードし、最終結果をPDBにアップロードします。
殆どのテストステージでlocaldbを使いますので必ず使い方を覚えましょう
##localdbブラウザーの開き方
localdbはウェブブラウザから見ることができます。ここではPreProducitonに向けてREPICのlocaldbについて説明します。
### REPICからlocaldbにつなげる場合
URL : http://192.168.100.104:5000/localdb/


![localdb](images/localdb_brower.jpg)

# Module QC Electrical Test
QC test結果はlocal dbにアップロードし、データの適正確認を行う。
