# モジュール読み出しQC手順

## まず始めに

### 基本操作方法(LV,HV)

基本、モジュールの読み出しを行う際はLV,HVVをＯＮにして行います(*ZeroBias Scanは例外 :HV OFF)。
LV/HV操作はpidClient.pyを実行することでできます。

```
$pidClient.py #PATHは通っているのでどのディレクトリでも実行可能
[admin@repicdaq1 ~]$ pidClient.py 
Press Ctrl-D to close the prompt.
 Type help to glance guides.
CoolingBox >>> help

Documented commands (type help <topic>):
========================================
EOF          relaunch_server   setHV_On      setMaxD         setTimeOffsetI
exit         resetDCS          setIduration  setMaxI         setTimeOffsetP
help         resetMPOD         setKd         setMaxP         setVerbosity  
kill_server  resetV            setKi         setRelayOff     stopPID       
log          restartPID        setKp         setRelayOn      unlock        
params       restoreInterlock  setLV_Off     setT          
quit         setHV_Off         setLV_On      setTimeOffsetD

LV ON command ::: CoolingBox >>> setLV_On
LV OFF command ::: CoolingBox >>> setLV_Off
HV ON  command ::: CoolingBox >>> setHV_On
HV OFF command :::  CoolingBox >>> setHV_Off
```

## 環境変数の登録

モジュールをCooloingBoxに設置後、解析ディレクトリーに移動.

```
$cd /nas/daq/QCtest/module-qc-operator
```

解析ディレクトリー上にあるsetup.shを実行

```
$ ./setup.sh -c configs/setup/repicdaq1_setup.json　# json fileはDAQ毎に分けられている DAQ1 : repicdaq1, DAQ2 : repicdaq2, DAQ3: repicdaq3
```

ここで引数-c config/setup/repicdaq1.jsonはDAQのhostnameに対応したものをそれぞれ選択。  
実行後環境設定を変更するか聞かれるのでyを選択しmoudleSN,config typeをそれぞれ選択。  

```
> Do you want to change env setting ? (y/n)
y
Input module SN (default:20UPGM22601029 ) : 
> set moduleSN : 20UPGM22601029
Config Type :(default :L2_warm)
0 : L2_warm
1 : L2_cold
>set config type : L2_warm
```

config typeは２種類あり20度でのElectrical testを warm test (L2_warm), -15度でのtestをcold test (L2_cold)となっている。  
最初は warm testなので 0:L2_warmを選択。  

* 環境設定は env/.repicdaq1_envに書き換えられる(環境設定の詳細を変更する場合は直接env fileを修正するとよい)。  

## config fileのダウンロード

次にlocalDBからconfig fileをダウンロードを行う。  
*Config fileは強制上書きされるので注意  
config fileは /nas/daq/QCtest/moduleディレクトリー上のmoudleSNに保存されるので要確認

```
$./ConfigDownload.sh 
======== Defolt Env Setting =============
moduleSN     : 20UPGM22601029 <- moduleSNが正しいか確認!
Config Type  : L2_warm
INSTITUITON  : HR <- HR :ハヤシレピック
LocalDB_HOST : 192.168.100.104 
LocalDB_PORT : 5000
HV Setting   : -80 [V]
=========================================
Config Type
0  all (L2_warm + L2_cold + L2_LP)
1  L2_warm
2  L2_cold
3  L2_LP
#ダウンロードするconfig fileを指定(default : 0 )
$0
Input accessCode1: #ATLAS PDBのaccessCode1を代入
Input accessCode2: #ATLAS PDBのaccessCode2を代入
```

ダウンロードが完了したら/nas/daq/QCtest/moduleディレクトリー上にmoudleSNに保存されいるか確認


## Zero-Bias Scan

zero bias scanはHVを印加していない状態でデータ取得を行う必要があるため、最初にする必要がある。
モジュールのconfig fileをダウンロードした後、LVをpidClient.pyでOnにする

```
$pidClient.py
$>> setLV_On
```

LVをOnにした後、GrafanaでLVの電流値/電圧、温度が適切であるか確認すべし  
電流値:5.88 A, 電圧値2.2V前後 (2.0付近の場合、configが通らないことがあるので注意)  
その後、zero bias scanを実行

```
$./lib/Scan.sh -m 15 -W TEST
```

## IV scan (HV)

zero bias scanのデータが取得できたら IV scanを行う。  
![moudle-qc-nonelectrical-gui (IVcure)](images/IV_curve_at_QCtesting.jpg) 
```
$./IV_scan.sh # 引数なし GrafanaでHVの電圧値が変化している様子が確認できる(0V -> -200 V)  
# IV scanが終わると自動的にGUIが開くよ(リモートでsshで実行するとGUIが表示されないことがあるので注意)
>>GUI上でuser : itkqc, PW : REPIC daqと同じを入力
>> moduleSNを入力
# ここでIVscanモードになっていることを確認 -> IVscanの選択肢が無い場合はmoduleのステージがInitial Warmになっているかlocaldbで確認
>>localdbにアップロードする IV scan結果(json file)を選択
# /nas/qc/ivcurve/repicdaq1/moduleSNディレクトリー上のjson fileを選択
>> アップロードを選択して完了
```

アップロードが完了するとlocalDB上でIVscan結果を確認（80 VでBreak Downしていないか）  

##Electrical QC test
QCテストは大きくわけて5つの項目があります

### Simple scan

1. ADC-CALIBRATION

2. ANALOG-READBACK (FEChip毎にテスト：計４回)

3. SLDO (FEChip毎にテスト：計４回)

4. VCAL-CALIBRATION

5. INJECTION-CAPACITANCE

6. LP-MODE

7. OVERVOLTAGE-PROTECTION
* ANALOG-READBACK, SLDOテストはChip毎にテストを行います(Chip1->Chip2->Chip3->Chip4)。
  どのChipがテストされているかはGrafanaで確認。

SimpleScanOperator.shを実行することでSimple Scan全て実行・解析・localdbへのアップロードをすることができます。

```
$./SimpleScanOperator.sh 
#---解析完了
> Do you want to update config file? (y/n)
```

解析が完了するとlocaldbに結果がアップロードされているのでそれを確認  
問題がなければy, テストに失敗した場合はnを入力

```
>> "> proceed to next_qc_test ? (y/n):　#解析が完了するとlocaldbに結果がアップロードされているのでそれを確認し、問題がなければ次のテスト項目へ進む
y : 次のテスト項目に進む (localdbにアップロードされた結果に問題がない場合)
n :　強制終了 (localdbにアップロードされた結果に問題がある場合)
```

###途中でSimple testを中断（強制終了）した場合
Simple Scanの途中で中断したり、テストが失敗した場合、SimpleScanOperator.shの-sオプションで続きからテストを開始することができます。

```
$./SimpleScanOperator.sh -h
 -s    : Start Test Label 
 0: ADC-CALIBRATION, 
 1: ANALOG-READBACK, 
 2: SLDO, 
 3: VCAL-CALIBRATION,
 4: INJECTION-CAPACITANCE, 
 5: LP-MODE, 
 6 : OVERVOLTAGE-PROTECTION
 # SLDOからテストを開始する場合
./SimpleScanOperator.sh -s 2
```

### 特定のSimple Scanだけをテストしたい場合

ScanOperator.shはSimple Scanテストをまとめて実行するスクリプトです。  
ある特定のsimple scanをテストしたい場合はlib/SimpleScan.shを使います。
SimpleScan.shの引数情報は-hで確認することができます。

```
$./lib/SimpleScan.sh

 -m    : Module QC Tool Mode
          0 : ADC-CALIBRATION
          1 : ANALOG-READBACK
          2 : SLDO
          3 : VCAL-CALIBRATION
          4 : INJECTION-CAPACITANCE
          5 : LP-MODE
          6 : OVERVOLTAGE-PROTECTION

##SLDOだけをテストしたい場合
$./lib/SimpleScan.sh -m 2
```

### ANALOG-READBACK, SLDOが失敗した場合

ANALONG-READBACK, SLDOはconfigのEF chip enable情報を操作してchip毎にスキャンを走らせます。
途中でテストを切り上げた場合はconfig設定を修正する必要があります

"chip の Enable/Desable の変更"

を参照してください。

とします．

（エディタで直接connectivity configをいじることは非推奨とします．）




### Minimum Health test

1. Digital Scan
2. Analog Scan
3. Treshold Scan (high range, low def)
4. ToT scan, 6000e injection charge

### Tuning Performance

1. Global threshold tune @2000e
2. Threshold Scan (high range, low def)
3. ToT scan, 6000e injection charge
4. Global threshold tune @2000e
5. Pixel threshold tune  @2000e
6. Global threshold re-tune @1500 (L2)
7. Pixel threshold re-tune @1500 (L2)
8. Threshold Scan (high def, low range)
9. ToT scan, 6000e injection charge

### Pixel Failure test

1. Digital Scan with clear mask
2. Analog Scan
3. Threshold Scan (high res, low range)
4. Noise Scan (10M trigger)

### Full Pixel Failure Analysis

1. Disconnected bump scan
2. Merged bump scan #現在必要なし
3. Treshold scan with no bias #最初に実行
4. Source scan (X-ray) #X線を照射した状態で実行

### Advavnce Scan

1. disconnected bump scan
2. zero-bias scan
3. xray scan

Minimum Health Test, Tuning Performance, Pixel Failure Analysis はScanOperator.shを用いて一通りテストすることができる。
テストが一つ終わるとプロセスが途中で止まるのでlocaldbで結果を確認してください。  
問題がなければ次のテストに進み、テストが失敗していたら終了しましょう。  

```
* Minimum Health test
$./ScanOperator.sh -W MHT # MHTはMinimum Health Testの略
* Tuning Performance test
$./ScanOperator.sh -W TUN
* Pixel Failure Analysis
$./ScanOperator.sh -W PFA
# スキャン完了
> proceed to next scan ? (y/n): 
# テストが一つ終わったのでlocaldbで結果を確認
- テスト結果に問題なし -> y (次のテスト開始)
- テスト結果に問題あり -> n (終了)
```

### Advance scan

こちらは一つずつコマンドを入力する必要がある

```
./lib/Scan.sh -m 11 -W TEST 
# disconnected bump scan : -m 11
# zero-bias scan         : -m 13
# xray scan              : -m 14
```

###途中でテストが失敗した場合
ScanOperator.shで途中でテストが失敗し中断した場合、
lib/Scan.shのシェルスクリプトで一つ一つテストを選択することができます。

```
./lib/Scan.sh -h 
./lib/Scan.sh: illegal option -- h
Help: Scan.sh [OPTIONS]....
  -W    : Enable using Local DB <str> scan mode (MHT, TUN, PFA, TEST etc.)
  -m    : Scan Mode
        : 0 : digital scan
        : 1 : analog  scan
        : 2 : threshold_hr scan
        : 3 : tot     scan
        : 4 : global tune scan
        : 5 : pixel  tune scan
        : 6 : global retune scan
        : 7 : pixel  retune scan
        : 8 : threshold_hd scan
        : 9 : digital scan clean mask 
        :10 : noise scan
        :11 : dis connect scan 
        :12 : cross talk scan
        :13 : threshold scan w/o HV
        :14 : xray scan
        :15 : no bias scan tuning
        :16 : binaly search
        :17 : cml bias scan

# 選択したいテストを数字で選択
$./lib/Scan.sh -m 4 -W TUN # (global tune scan) -Wを付けないとlocaldbにアップロードされないので注意！ 
```

###テストが修了したら

local DBで各試験を登録する必要があります。  

1. 試験したmoduleを構成するFront End Chipのページに行き、Minimal Health Test, Tuning, Pixel Failure AnalysisのCheckout Scansをそれぞれクリックする  
2. 右側のtagとDateを参考に該当するrunを選択する。  
3. 一番下のProceedを選択。上のチェックマークをつけてあれば、1 chipで登録すれば同じrunがすべてのchipで登録される。  
4. 全てのFront End Chipで1.で設定した3の項目が緑になっていることを確認する。  
5. 試験したmoduleのページに戻り、Electrical Test Module SummaryのCreate Summaryをクリックする。  
6. 最終結果とするテスト項目を選択。  
7. 一番下のProceedを選択。Local sign offかITK PDにアップロードするかはシフトリーダーに確認のこと。  

###Coldテストが修了したら

マスクなしで完遂できればQC Sucsess、一部をマスクしてスキャンを完遂できたならConditional Pass、完遂できなければQC Failed。詳しい基準や個別事象の判断はシフトリーダーと相談のこと。

# 困ったときのメモ (debug)


## CML Biasのデフォルト値の変更

CML Biasのデフォルト値を変更するには，
```
$ ./lib/changeCmlBias.sh [chipid] [BiasChannel [0,1,2]]  [NewValue]
```
を行います．`CmlBias1`を`200`に設定する場合は，
```
$ ./lib/changeCmlBias.sh all 1 200
```
とします．chip毎に個別したい場合は 一つ目の引数にchip番号を使います。(例、chip1の `CmlBias1`を`200`にする)
```
$ ./lib/changeCmlBias.sh 1 1 200
```

`2023-08-23`現在，`CmlBias1`のHRでの推奨値は`200`とします（初期値：`400`）．
→ First digital scanの結果が不良の場合にはCmlBias1の値を`100`程度まで下げて改善するか見てください．

## chip の Enable/Desable の変更

YARRのスキャン等で、1チップだけのスキャンを行いたい。とか、悪いチップをDisableしたいといったときは以下のコマンドを使ってください。
```
$ ./lib/ModifyChipEnable.sh [status]
```
[status]は、4chipに相当する4桁の1/0の文字列で指定できます。1... enbale 0...disable 
例えば、chip 2だけdisableしたい場合は、
```
$ ./lib/ModifyChipEnable.sh 1011

```
で可能です。
（エディタで直接connectivity configをいじることは非推奨とします．）

## First Digital Scan

まず，`digitalscan`を走らせて通信に問題がないか確認します．
```
$./lib/Scan.sh -m 0 -W TEST
```

## Local Sign-offを行ったときのLocalDBでのFE configの作成

Local Sign-offを行って次のステージに行った場合は，前のステージのconfigを引き継いで，LocalDBにFE configのスタートポイントを作成する必要がある．

例は以下の通り（`repicdaq[1-3]`で実行します）：

```
cd /nas/qc/jp-itkpd-scripts/localdb
python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/INITIAL_WARM"  "MODULE/INITIAL_COLD"
```

各Front End ChipのConfig Revisionsで適切なStage名の下にMode: warm, cold, LPのエントリがそれぞれあれば適切に作成できている。

