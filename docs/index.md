# はじめに

## このマニュアルについて

このマニュアルの内容は作業手順の最適化や、システムの改善などで常に変更されていくことが期待されます。
また、シフト歴などが低い人の観点からは内容が理解しにくい部分などの改善は特定の属性の人からフィードバックが不可欠です。
そのため、定期的に自分の過去の知識とマニュアルの内容のすり合わせることや、貴方自身が編集して良いものにしていくという意識を持って貰えるとよいかと思います。
更新は非常に簡単なので、何か内容を更新したい点などに気づいたらご自身で変更をよろしくお願いします。

### 変更の仕組み
以下のgit repositoryにソースコードが置かれており、マークダウン形式で文章が作られています。

[https://gitlab.cern.ch/atlas-jp-itk/module-qc-doc-japan](https://gitlab.cern.ch/atlas-jp-itk/module-qc-doc-japan)

ここに保存されているmasterブランチのソースコードが、GitのCI/CD機能によって自動的に公開される仕組みになっています。
なので、編集したい場合は、自身のパソコンにcloneしてきて編集するか、GitLabのWeb IDEなどを用いて編集後、commit->pushした後、masterへマージ申請してください。

### Web IDEを用いた編集方法

* 上記gitlabのlinkをたどり，ページ右上のLinkからsign inする
* 左側面menuからRepository/Filesを選び，docsフォルダを選ぶ。編集したいファイル(.md形式)を選択するとShifter manualページと同様の内容が見える。
* 上部に”Open in Web IDE”というメニューがあるので選択すると，Visual Studio Code風の編集画面になる。
* 編集が終わったら，左の側面にあるメニューで丸数字（①）があるアイコンを選ぶと Commit & Pushというメニューがでる。上にあるComment messageに更新する理由を書き込む。
* Yes (commit to a new branch)/No (Use the current branch master' とでるので Yesを選ぶ。そして Press 'Enter'
* 右下にポップアップがでて，'Create MR','Go to Project','Continue working'とでるので
ここで追えたければ'Create MR'を選ぶ。続けたければContinue workingを選ぶ。
* 最後に確認ページに幾つか情報を書き込めば終わりだよ。

## 館山専用Skype

夕食や集合等の連絡用に使います．館山に来たら参加してください．

（館山から帰ったら，退出して構いません．）

[https://join.skype.com/qxgpOPcM6i3i](https://join.skype.com/qxgpOPcM6i3i)

## 館山専用Zoom

館山作業でZoomが必要の場合に随時使用してください．

[https://cern.zoom.us/j/63584339587?pwd=cUhUNytRaDNUYVMwUXdGamtNSUp4dz09](https://cern.zoom.us/j/63584339587?pwd=cUhUNytRaDNUYVMwUXdGamtNSUp4dz09)

## クリーンルームに電話する

以下のSkypeで部屋のメトロロジーマシンに直接電話できます．
[https://join.skype.com/invite/ykEvn8AZzdYv](https://join.skype.com/invite/ykEvn8AZzdYv)

## 部屋に入る前に

館山工場は企業の作業員の方々が黙々とお仕事をしている場所で，僕たちは場所をお借りして作業をさせてもらっています．

* 作業員の方々が休憩する次の時間の前後にクリーンルームから出入りすることは避けるようにお願いされています：8:30 - 8:45, 10:00 - 10:10, 12:00 - 12:40, 15:00 - 15:10, 17:20 - 17:30

* 昼食時に電子レンジを借りることができますが，食堂のスタッフに一言声をかけてください．作業員の方々が入る前には温め終えられるようにしよう．

* トイレは来賓用，社員用のどちらも使うことが許可されています．

!!! note
    食堂の自販機は自由に使用して構いません．

## クリーンルームの出入り

* クリーンルームに持ち込むものは繊維質でないもので，かつ最小限に絞ってください．
* 作業初日に，その週に使うウェアとシューズのサイズを段ボール箱から選びます．
* ウェアとシューズの開封は着替えスペースで行います．
* 脱いだウェアとシューズは入り口はいって右側の縦長のロッカーにしまいます．
* 他の人と区別がつくように，着替え室の入口側に入っている番号札をつけておいてください（つなぎとシューズに１つずつ）．

![クリーンルーム・ウェア](images/cleanroom_wear.png){align=right, width=150}

!!! note "気を配ろう"
    ウェアを着たときに，鏡で**髪の毛がはみ出ていないか**必ずチェックしよう．

* クリーンルームには**エアシャワーつきの二重扉**から入るよ．
  → 入るときに中から人が出ようとしていないか確認しよう．
* エアシャワーは，外側の扉が「先に」開閉した場合に約20秒作動するよ．

!!! warning "注意"
    内側の扉が「先に」開閉した場合は作動しないよ．この順番が出入りする向きと逆になってしまわないように，前の人が扉を完全に閉じたのを確認してからドアを開けよう．

* 素手で触れてはいけないものにアクセスするときに，必ず**指サックを着用**してください．クリーンルーム内に大小サイズで常備してあります．

![指サック](images/yubi_sack.png)

## 館山のシステム概説

![クリーンルーム外観](images/overview.png){align=left, width=100%}

### コンピュータ・ネットワーク

#### LANネットワーク

| IP address      | ニックネーム    | 役割                                                                |
| --------------- | --------- | ----------------------------------------------------------------- |
| 192.168.100.101 |           | NAS                                                               |
| 192.168.100.102 |           | NAS (冗長)                                                          |
| 192.168.100.103 | repicmet  | メトロロジー                                                            |
| 192.168.100.104 | repicdb   | リモートログインのGateway, LocalDB, 閉鎖ネットワークのDHCP, 室内温湿度, Particle Counter |
| 192.168.100.105 | repicdaq1 | CoolingBox DAQ-#1                                                 |
| 192.168.100.106 | repicdaq2 | CoolingBox DAQ-#2                                                 |
| 192.168.100.107 | repicdaq3 | CoolingBox DAQ-#3                                                 |
| 192.168.100.6   | repicmon1 | 恒温槽・デシケータ・露点監視                                                    |

##### [かんたん解説]

* NASサーバがQCに関わる共有データを他のマシンにファイル共有しているよ！とっても大事なサーバーで，データのバックアップもばっちりです．それぞれのユーザの    ホームディレクトリもここに保存されてるよ！ 
* NASサーバはネットワーク内共通で使えるユーザアカウント（LDAP）も管理しているよ！ただし`repicmet`はWindowsマシンなので別になっているよ．
* `repcdb`は外部からリモートログインを行うことができるから，めちゃ便利！

!!! question "アカウントの作成"
    アカウントが未作成の人は阪大の[廣瀬穣さん](mailto:hirose@champ.hep.sci.osaka-u.ac.jp)に希望アカウント名を添えてコンタクトしてください．

!!! question "リモートログインの方法"
    リモートログインについてはセキュリティの問題があるから，その方法はここでは公開しないよ．やり方は近くにいるスタッフに直接聞いてね！

!!! note "LANネットワークへの接続"
    自分の持ち込みPCをLANに接続することができます．やり方は近くにいるスタッフに直接聞いてね！
    また，クリーンルーム内のディスプレイやマウスを自分のPCに接続して作業できます．

#### 閉鎖ネットワーク

| IP address | ニックネーム | 役割                  |
| ---------- | ------ | ------------------- |
| 10.0.0.1   |        | repicdbと同一          |
| 10.0.0.2   |        | MPOD Crate          |
| 10.0.0.3   |        | Particle Counter #1 |
| 10.0.0.4   |        | Particle Counter #2 |
| 10.0.0.5   |        | repicdaq1と同一        |
| 10.0.0.6   |        | repicdaq2と同一        |
| 10.0.0.7   |        | repicdaq3と同一        |

##### [かんたん解説]

- MPODはモジュールにLVやHVを供給するクレートなので，間違って操作できないように，閉鎖ネットワークの中にいるよ．`repicdb`および`repicdaq[1-3]`はMPODを制御する必要があるから，LANネットワークとは別にUSB-Ethernetアダプタを介して閉鎖ネットワークにも接続されているんだ．
- MPODはかんたんに固定IPアドレスを名乗れないので，`repicdb`が閉鎖ネットワークのためのDHCPサーバとして働いているよ！

![Netowrk Model](images/network_model.png){align=right}

#### SSHポートフォワーディング

- 以下のような設定を自分の端末の`~/.ssh/config`に描いておくのがおすすめです．
  
  ```
  Host repicgw
    Hostname スタッフに確認のこと
    User あなたのユーザ名
    Port スタッフに確認のこと
    LocalForward 8080 192.168.100.101:8080 # QNAP Admin
    LocalForward 4444 192.168.100.101:443  # QNAP File Sharing
    LocalForward 3000 192.168.100.104:3000 # grafana server
    LocalForward 5000 192.168.100.104:5000 # LocalDB
    LocalForward 5100 192.168.100.104:5100 # HR Database
    LocalForward 27017 192.168.100.104:27017 # MongoDB
    LocalForward 5904 192.168.100.104:5901 # repicdb VNC (user:admin)
    LocalForward 5905 192.168.100.105:5901 # repicdaq1 VNC (user:admin)
    LocalForward 5906 192.168.100.106:5901 # repicdaq2 VNC (user:admin)
    LocalForward 5907 192.168.100.107:5901 # repicdaq3 VNC (user:admin)
  ```

- このような設定で
  
  ```bash
  $ ssh repicgw
  ```
  
  のようにログインしたあと，ブラウザで，
  
  ```
  http://localhost:3000/
  ```
  
  に接続するとGrafanaをブラウズすることができるよ！

![Port_Fowarding](images/ssh_portfwd_grafana.png){align=right}

#### 各種Webサービス

| URL                                                                                        | Name            | 役割             |
| ------------------------------------------------------------------------------------------ | --------------- | -------------- |
| http://192.168.100.104:3000                                                                | Grafana server  | 時系列データの監視      |
| http://192.168.100.104:5000/localdb                                                        | LocalDB Viewer  | QCデータの管理       |
| http://192.168.100.104:5100/repicdb                                                        | LocalDB Viewer  | REPIC側QCデータの管理 |
| 192.168.100.104:8086                                                                       | InfluxDB server | 時系列データの監視      |
| http://192.168.100.6/                                                                      | Thermal Cycling | 恒温槽熱サイクルの制御    |
| 192.168.100.103:5900                                                                       | Metrology       | UltraVNC       |
| [192.168.100.101](https://192.168.100.101/share.cgi?ssid=7b7f9eab1e9e42159a79818747b27db8) | NAS             | File Browser   |

#### Metrology Machine VNC access

一例は以下で，自分のマシンの`15901`番にフォワードします：

```
ssh -L 15901:192.168.100.103:5900 -L 3389:192.168.100.103:3389 <username>@<REPIC_HOSTNAME> -p <REPIC_PORT>
```

#### Expert Log

設定時のElogは[こちら](http://atlaspc5.kek.jp/elog/ATLAS+pixel/52)

#### シフター用 e-Log

シフトの記録は[こちら](http://atlaspc5.kek.jp/elog/ATLAS+pixel/)

* 同時編集はできないので編集したい場合は他のシフトクルーに声を掛けて，編集中のドラフトがないことを確認するといいよ。
* 編集の保存は'save'ボタンで，一日の最後に’submit’をするとログが閉じるよ。


### ガスライン

![乾燥窒素ライン](images/nitrogen_line.png){align=right, width=150}

##### [かんたん解説]

館山工場には乾燥空気と乾燥窒素の２種類のガスラインがあるよ．モジュールQC作業では主に乾燥窒素のガスラインを使うよ．以下のことを覚えておこう．

* 乾燥窒素はデシケータと恒温槽にはつねに一定量で流れているよ．それぞれの湿度は常時モニタされている．
* 恒温槽に流す乾燥窒素はモレキュラーシーブで露点を改善しているよ．
* ３台あるCoolingBoxにも乾燥窒素が分配されているよ．それぞれに流す流量はだいたい1-2 L/min程度だけど，モジュールの設定温度によって微調整することがあるよ．

### 電力

![電源系統](images/power_line.png){align=right, width=150}

##### [かんたん解説]

まずいちばんはじめに覚えておくことは，次の２点だよ！

* 電圧にはAC100V系統とAC200V系統の２種類がある
* それぞれの電源系統には制限容量がある

ITkPix Moduleの製造では恒温槽やチラーといった大きな電力を使う機器もたくさんあるよ．なので，それぞれの系統の制限容量内に定格が収まるように，正しく分散して機器を接続しています．

!!! note
    新しく機器をつないだり，自分のPCの電源を一時的に取りたいときなどは，事前にスタッフに確認するようにしよう．

### デシケータ

##### [かんたん解説]

デシケータは製造前後やテスト中のモジュールをストックしておく棚で，湿度管理が徹底されているよ．前面の扉にセンサーがついていて，いつ扉が開閉したかを記録しているよ．

棚の中の湿度は普段は0.3%とかだけど，扉を開けるとあっという間に部屋の湿度にまで戻ってしまう．乾燥窒素を循環して回復するには数時間かかるから，できるだけ扉を開ける時間が短くて済むように気をつけよう．

### アセンブリ・ジグ

→ くわしい作業マニュアルは[こちら](assembly)

### 顕微鏡システム

![顕微鏡システム](images/metrology_system.png){align=right}
![顕微鏡システム](images/metrology_chuck.png){align=right}

##### [かんたん解説]

* モジュールやPCBの詳細な写真を撮影してVisual Inspectionを行うよ！
* モジュールやPCBの長さや厚さを正確に測定して規格に適合しているかをチェックするよ！

→ くわしい操作マニュアルは[こちら](metrology_microscope_manual)

### 恒温槽

→ くわしい操作マニュアルは[こちら](thermal_cycling)

### Testing System

![Cooling Boxes](images/cooling_boxes.png){align=right}

![Cooling Box zoomed](images/cooling_box_zoomed.png){align=right}

##### [かんたん解説]

* 複数のCoolingBoxが並列に作動するようにできているよ．
* CoolingBoxはモジュールの温度-15℃以上の一定値で動作するように温度制御を行うよ．
* 安全に試験ができるように，湿度管理を気をつけているよ．
* 誤作動や誤操作を起こしたときにモジュールを破損しないように様々な保護機構（インターロック）を備えているよ．
* １台のCoolingBoxにつき１台のPC (repcdaq[1-3])でDCSとDAQをコントロールしているよ．

→ くわしい操作マニュアルは[こちら](cooling_box_manual.md)

#### MPOD電源

![MPOD電源](images/mpod.png)

##### [かんたん解説]

* モジュールにLVとHVを供給するよ！
* LVモジュールは8chあるよ
* HVモジュールは16chあるよ

#### X線照射装置

![X線照射装置](images/xray_box.png)

##### [かんたん解説]

* 最大50kV, 80µAの出力でX線を発生できる小型装置だよ
* CoolingBoxの上にかぶせて上からモジュールにX線をあてて，わずか数分で全ピクセルのバンプ接続状況を確かめることができるよ！
* フロントパネルのタブレットでかんたんに操作ができるよ
* 安全にはくれぐれも気をつけて，手順をしっかり守って操作しよう！

→ くわしい操作マニュアルは[こちら(PDFをダウンロード)](https://gitlab.cern.ch/atlas-jp-itk/xray-system-manual/-/raw/master/main.pdf?inline=false)
