# 組み立て後のモジュールの形状測定

## 基本的な注意事項

測定は以下の順番で行う。(2023/08/01現在)

* アセンブリ後 (ASSEMBLY)
  
  1. 表面の撮影 (LED off)
  2. 表面の撮影 (LED on)
  3. 表面のサイズスキャン
  4. 裏面の撮影 (LED off)

* ワイヤーボンディング後 (WIREBONDING)
  
  1. 表面の撮影 (LED on)

表面の撮影、メトロロジーの際にはポジション1に、裏面の撮影の際にはポジション3にモジュールをセットする。

アセンブリ後モジュールの測定時には、ポジション1のステージにボトムフレームをはめ、その上にモジュールを設置する。

![ポジション1モジュールなし](images/position1_nomodule.png){align=right, width=400}
![ポジション1モジュールあり](images/position1_module.png){align=right, width=400}

ワイヤーボンディング後モジュールの測定には、ポジション1のステージにモジュールキャリアごと設置する。この際、測定対象モジュール側のモジュールキャリアのボトムフレームは取り外す。

モジュールの設置、取り外しをする際には、常に「Replace Position」で、真空吸着をoffにして行う。ステージを動かす際には、モジュールが滑るのを防ぐため、真空吸着がonの状態で行う。

!!! note "結果の写真のありか"
    /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]

    module-qc-nonelec-gui などで使います。

撮影された写真は、ローカルディスク__（パスはどこ？）__と、Share Link (https://192.168.100.101/share.cgi?ssid=7b7f9eab1e9e42159a79818747b27db8) のPreProduction\Module以下の、各モジュールのシリアル番号に対応するフォルダーにアップロードされる。

## ステップバイステップの測定手順
（2023/08/01時点での手順。手順やGUIの変更などがあれば、その都度変更してください）

表面の撮影。

1. ポジション1にボトムフレームが設置されていることを確認。設置されていなければ、設置する。
2. モジュールをポジション1に設置する。
   * GUI上でVacuumがoffになっていることを再度確認。
   * まず、位置決めピンを_右上_と_左下_に挿入する。
   * モジュールを設置する。

!!! note "モノは所定の位置に！"
  キャリアはキャリア置き場に、位置決めピンはピン入れに常に置こう。

3. GUIにモジュールのシリアル番号などを入力する。
   * SN欄にシリアル番号を入力.入力欄をクリックしカーソルを表示した状態で、キャリア蓋にあるQRコードをスキャンすると、自動入力。SETをクリックし、コンポーネートタイプをセット。
   * Stage欄から「ASSEMBLY」を選択。
   * Type欄から「PreProduction」を選択。
   * 「Set Scan list」をクリックし必要なテストがチェックされているか確認。
   * 「Start」をクリックすると、音が鳴り自動でスキャンが進行する。
   * 音が鳴るたび作業の指示が促されるので、それに従い進めていく。
   
    スキャンは自動で、表面撮影（LEDなし）、表面撮影（LEDあり）、形状測定の順に進む。
   
!!! warning
	ASSEMBLYステージでは裏面を撮るため，吸着後， __ピンを抜く__ こと．
    一方，WIREBONDINGステージではワイヤ保護のため __ピンを抜かない__ こと。
    これらは途中でポップアップ画面で指示があるので見逃さないように。指差し確認推奨。
   
4. それぞれのスキャンが終了すると、自動でスティッチングスクリプトが走り、スティッチング後の写真が別Windowsで表示される。正しく撮影できていることを確認したら「×」をクリック。「Do you want to save this data to disk server as background」と聞かれたら、「はい」と答えると、写真がShare Linkにアップロードされる。
   * もし問題があれば、「いいえ」をクリックし、Single Scanをやり直す。

サイズスキャン中には、以下の写真を撮影する。治具表面とFlex pickup areaは4枚中最低3枚、Flex fiducial marksは4枚すべてにフォーカスがあっていることを確認すること。

治具の表面（高さの基準）
![治具表面](images/ref/ref_module_jig_s.png){align=right, width=800}

Flex pickup area
![治具表面](images/ref/ref_module_flex_pickup_s.png){align=right, width=800}

Flex fiducial marks
![Flex fiducial marks](images/ref/ref_module_flex_fmark_s.png){align=right, width=800}

ASIC fiducial marks
![ASIC ucial marks](images/ref/ref_module_asic_fmark_s.png){align=right, width=800}

HV capacitor
![HV capacitor](images/ref/ref_module_hvcapacitor_s.png){align=right, width=800}

Powerコネクタ
![Powerコネクタ](images/ref/ref_module_connector_s.png){align=right, width=800}

ASIC右辺
![ASIC右辺](images/ref/ref_module_asic_right_s.png){align=right, width=800}

Sensor下辺
![Sensor下辺](images/ref/ref_module_sensor_bottom_s.png){align=right, width=800}

ASIC左辺
![ASIC左辺](images/ref/ref_module_asic_left_s.png){align=right, width=800}

Sensor上辺
![Sensor上辺](images/ref/ref_module_sensor_top_s.png){align=right, width=800}

Flex左辺
![Flex左辺](images/ref/ref_module_flex_left_s.png){align=right, width=800}

Flex上辺
![Flex上辺](images/ref/ref_module_flex_top_s.png){align=right, width=800}

Flex右辺
![Flex右辺](images/ref/ref_module_flex_right_s.png){align=right, width=800}

Flex下辺
![Flex下辺](images/ref/ref_module_flex_bottom_s.png){align=right, width=800}

(Below, ONLY for the ASSEMBLY stage)

5. 表面の測定が終了した後、裏面測定のためにモジュールをひっくり返す操作をASSEMBLYステージでは要求される。ポップアップの指示に従えば良い。以下は要約。

6. 右側の治具を左側の治具に被せるようにしてゆっくり下す。4つの真空吸着がFlex表面のpickup pointに触れていることを確認したら、真空吸着を下側の治具から上側の治具に切り替える。このとき、モジュールが上下両方で引かれないように、下記の順番で切り替える。
     * まず、下側治具の青いつまみを回して吸着を閉じる。
     * つぎに、上側治具の青いつまみを回して吸着を開ける。

![治具着陸後](images/placed.png){align=right, width=400}

!!! warning
	  Moduleが落ちる可能性があるため以下の作業は慎重に！！

  7. 上側の治具をゆっくり持ち上げ、元の位置に戻す。この時点で、モジュールは裏面が上を向いた状態でポジション3に自動的にセットされる。

![治具運搬中](images/flipped.png){align=right, width=400}

  8. ポップアップウィンドウの「はい」をクリックすると、裏面の撮影が始まる。
  9. 裏面撮影が終わったら、表面測定の手順4. を実行する。

以上で、アセンブリ直後の測定は終了。ITkPixモジュール作業票の「アセンブリ終了後」の「写真撮影（表、裏）」「測量（メトロロジー）」がすべて終了。続いて、module-qc-nonelec-guiで外観検査、メトロロジデータ解析し、問題がないかを確認してデータベースに上げたのを確認。必要項目をチェックシートに記入しシフトリーダにチェックシートを確認してもらう。
この後、REPICの方に引き渡し、ワイヤボンディングをして貰う。

![シート](images/sheet.png){align=right, width=400}

## ワイヤーボンディング後の測定

ワイヤーボンディング後には、表面の撮影のみを行う。

1. モジュールのシリアル番号などを入力する。
   * SN欄にシリアル番号をスプレッドシートで確認し、入力。
   * Stage欄から「WIREBONDING」を選択。
   * Type欄から「PreProduction」を選択。
   これで「Start」と書かれた緑丸をクリックすると、ステージが「Replace Position」に引き出され、真空吸着がoffになった上で、「Please set module with backside up」というメッセージが表示される。
2. ポジション1にボトムフレームが設置されていたら、それを取り外す。
3. モジュールをポジション1に設置する。
   * まず、モジュールキャリアのボトムプレートを外し、トッププレートはネジだけを外す。
   * モジュールをキャリアごとステージのポジション1に設置し、トッププレートを取り外す。
   ここまでできたら、ポップアップウィンドウの「はい」をクリックすると、スキャンが始まる。
4. アセンブリ直後の測定の手順4.　を実行する。

## スキャン・データの解析（形状測定）

### 環境設定とプログラムの実行


1. metrology machineの常駐プログラムリストなどで、Xmingが起動していることを確認。
![Xming](images/ss_xming.png){align=right, width=800}

タスクバーなどからubuntuアイコンを選択し、端末を開く。

!!! warning
    PowerShellなどで開いた端末だとX Window転送がうまく行かない！

2. repicdbへSSHログイン。個人のユーザでOK。

```
ssh -XY -l [your user] 192.168.100.104
```

!!! warning
    repicdaqXXではGUIがクラッシュします！必ずrepicdbで作業。


3. repicdb上で、以下のコマンドを実行し、解析用GUIを起動。

```
cd ~/
source /nas/qc/metrologyAnalysis/setup.sh
pmapp.py &
```

### スキャン・データ解析用GUI

プログラム（pmapp.py）を実行すると、以下のようなGUIが起動する。

- Component type
- Test stage
- Module name (Serial number)
  を入力して、
- サイズ測定のために取得したスキャン・ディレクトリの下にあるlog.txtファイルを選択する。

!!! note
  ファイルのパスは以下。
  /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_Size_Front/log.txt

- Log fileはSizeとHeightの両方に設定する。
  - 現状では、Size測定として取得したスキャンのlog.txtを両方に設定する。（2023.08.01現在）
- モジュールの情報とLog fileを設定したら、"Process All"ボタンを押して処理を開始する。

![pmapp.py GUI](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmma-gui2.png)

### 解析結果の確認

解析結果のまとめは中央の表に表示される(HeightとSizeのタブを確認)。設計値と許容範囲内の変数は緑、許容範囲外の変数は赤で表示される。色がついておらず黒字の変数は計算途中で使用したデータで、気にしなくてよい。

- Height測定
  - 通常、全て緑になるはず。
- Size測定
  - 自動のパターン認識では、現状赤がたくさん出る。その場合、手動でのパターン検出が必要である。

![高さ測定の表](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/height-summary.png)
![高さ測定の表](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/size-summary-red.png)

### 手動でのパターン検出

一番右のパネルに写真の一覧が表示される。それぞれの写真が検出しようとしているパターンを目で見て、ダブルクリックして点を打つことで自動検出の結果を上書きできる。画像の上でダブルクリックすると、緑色の丸が表示されこの座標が使用される。変なところに点を打ったら、もう一度ダブルクリックしてやり直す。点が更新されるたびに中央の表にも結果が反映される。

<details><summary>Asic Left</summary>
![ASIC Left](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-asicL.png)
</details>
<details><summary>Asic Right</summary>
![ASIC Right](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-asicR.png)
</details>
<details><summary>Sensor Top</summary>
![Sensor Top](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-sensorT.png)
</details>
<details><summary>Sensor Bottom</summary>
![Sensor Bottom](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-sensorB.png)
</details>
<details><summary>Flex Top</summary>
![Flex Top](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-flexT.png)
</details>
<details><summary>Flex Bottom</summary>
![Flex Bottom](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-flexB.png)
</details>
<details><summary>Flex Left</summary>
![Flex Left](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-flexL.png)
</details>
<details><summary>Flex Right</summary>
![Flex Right](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-flexR.png)
</details>
<details open="true"><summary>Fiducial marks</summary>
![Flex fiducial marks](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-flexFmark.png)
![Asic fiducial marks](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmm-asicFmark.png)
</details>

### 結果の保存

GUIで指定したComponent typeに応じて、MODULE等のディレクトリが/nas/qc/metrologyDataの中に作成され、その下に"001"のような形式で、同じcomponentに対して複数回データを処理した際にその都度新しいディレクトリが作成される。この作業場所には、解析途中で作成した加工済み画像などが保存されている。

データの処理と手動での補正が完了したら、"Save (create JSON file)"ボタンを押すと、作業場所にdb.jsonとdata.pickleというファイルが作成される。db.jsonはデータベースに上げるためのデータである。（要検証）data.pickleの方は、アプリケーションの実行時の情報を丸ごと保存したもので、より詳細な計算結果等にもアクセス可能である。

!!! note 
    GUI上では何も起こっていないように見えるけど。ファイルは出来ているはず。
    "/nas/qc/metrologyData/MODULE/[ATLAS_SN]/MODULE_ASSEMBLY/[Some Number]/db.json"
    が出来ているか確認するか、pmapp.pyを開いた端末にその旨のメッセージが出ているかを見ると良い。(改善されると親切)


## 外観検査とメトロロジ解析結果のアップロード

1. metrology machineの常駐プログラムリストなどで、Xmingが起動していることを確認。
2. タスクバーなどからubuntuアイコンを選択し、端末を開く。

!!! warning
    PowerShellなどで開いた端末だとX Window転送がうまく行かない！

3. 下記コマンドでrepicdaqXXへログイン(下はXX=1の例だがどのマシンでも良いはず)。
  ```
  ssh -l admin -XY 192.168.100.105
  ```

4. 下記コマンドを打ち、GUIを起動。
```
module-qc-nonelec-gui
```

5. LocalDBへのログイン(user: itkqc)

6. Visual inspectionとMetrologyをGUIに従って進めていく。必要なファイルは以下。
   * Metrology: /nas/qc/metrologyData/MODULE/[ATLAS_SN]/MODULE_ASSEMBLY/[Some Number]/db.json
   * Visual Inspection (FRONT): /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_VI_Front_vi.jpg 
   * Visual Inspection (BACK): /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_VI_Back_vi.jpg 
