# PCB Reception Test

KEKで全作業を行う。
作業はMetology Machine setupを用いる。
![KEKMetrologysetup](images/Metrologymachine.png){align=right,width=150}

SMD(抵抗などの各パーツ)の実装前後でBare Flex,Assembled Flexに大別され、それぞれで測定を行う。
## BareFlexMetrology

### 測定項目
* Photo scan(表面)
* Size scan
* Pattern Scan
* Photo scan(裏面)

## AssembledFlexMetrology

### 測定項目
* Photo scan(表面)
* Size scan

## FlexElectricalTest
Assembled FlexのMetrologyの後に行う。
1. Flexを袋から取り出してpig tailのコネクタを外す
1. ベース基板のアライメントピンに沿ってFlexを取り付ける。
flat cableコネクタが上に来る向きで設置。
1. 上のケーブルがついた基板を取り付ける。
Flexが通電してるとpico logのNTCの電圧が1.2V前後になっている。(通電していないと200mVとか)
このとき、ワニの部分をHVのコネクタにつけることを忘れないこと!
1. pico logでまず前の測定結果をreset
2. 録画の赤い丸のボタンを押してlocalで記録する。
測定時間を **21分** に設定して測定開始
3. グラフのボタンを押してHV LVをモニターできるようにする。
4. その後HVLVの電源を入れて測定開始。
5. GND+とGNDの電圧ドロップをみる。この間にCC 5Aがかかっており、電圧がおよそ400mV dropする。
このとき、picologに接続しているアンプで電圧を10倍にしているので 400/10 = 40 mV
よってこの間の抵抗は 0.04/5 = 8mΩ
6. 同様にVIN+とVIN-の電圧dropも観察する。ここの間の電圧dropは大体35mVで
35mV/ 5A = 7mΩ程度の抵抗がかかっている。
7. 紫のHVのlineはcapacitanceにかかっているHVで時定数がみれる。おおよそ最後のほうで100mVくらいの電圧でプラトーになればいい

### 参考資料
Bare Flex QC manual wrote by Mami Takagi
[https://cernbox.cern.ch/s/wJ27IG0J1Dvzpz7](https://cernbox.cern.ch/s/wJ27IG0J1Dvzpz7)

Assembled Flex QC manual wrote by Yuta Hiemori
[https://cernbox.cern.ch/pdf-viewer/public/fXoVK6zTKxn9Zjm/howtoDoFlexQC.pdf?contextRouteName=files-public-link&contextRouteParams.driveAliasAndItem=public%2FfXoVK6zTKxn9Zjm&items-per-page=100](https://cernbox.cern.ch/pdf-viewer/public/fXoVK6zTKxn9Zjm/howtoDoFlexQC.pdf?contextRouteName=files-public-link&contextRouteParams.driveAliasAndItem=public%2FfXoVK6zTKxn9Zjm&items-per-page=100)